package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;

@Service
public class StudentService {
	@Autowired
	private StudentRepository repository;
	
	public List<Student> getStudents() {
		return repository.findAll(); 
	}
	
	public List<Student> getStudentByName(String name) {
		return repository.findByNameContaining(name); 
	}
	
	public Student getStudent(int id) {
		return repository.findById(id).orElse(null);
	}
	

	public Student saveStudent(Student student) {
		return repository.save(student); 
	}
	
	public Student updateStudent(Student student) {
		return repository.save(student); 
	}
	
	public String deleteStudent(int id) {
		if(getStudent(id) == null) {
			return "Student not found !";
		}
		repository.deleteById(id);
		return "Student deleted " + id;
	}
	
	
	
}
