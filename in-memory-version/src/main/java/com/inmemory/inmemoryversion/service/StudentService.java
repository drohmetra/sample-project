package com.inmemory.inmemoryversion.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.inmemory.inmemoryversion.entity.Student;

@Service
public class StudentService {
	
	List<Student> studentList = new ArrayList<Student>();
	
	public List<Student> getStudents() {
		return studentList;
	}
	
	public List<Student> getStudentByName(String name) {
		
		List<Student> filterList = new ArrayList<Student>();
		for(Student student:studentList) {
			String sname = student.getName().toLowerCase();
			name= name.toLowerCase();
			for(int i=name.length();i<=sname.length();i++) {
				if(sname.substring(i-name.length(), i).equals(name)) {
					filterList.add(student);
					break;
				}
			}
		}
		return filterList;
	}
	
	public Student getStudent(int id) {
		
		for(Student student:studentList) {
			if(student.getRollNo() == id) {
				return student;
			}
		}
		return null;
	}
	

	public Student saveStudent(Student student) {
		studentList.add(student);
		return student;
	}
	
	public Student updateStudent(Student student) {
		for(int i=0;i<studentList.size();i++) {
			Student s = studentList.get(i);
			if(s.getRollNo() == student.getRollNo()) {
				studentList.set(i, student);
			}
		}
		return student; 
	}
	
	public String deleteStudent(int id) {
		if(getStudent(id) == null) {
			return "Student not found !";
		}
		for(int i=0;i<studentList.size();i++) {
			Student s = studentList.get(i);
			if(s.getRollNo() == id) {
				studentList.remove(i);
			}
		}
		return "Student deleted " + id;
	}
	
	
	
}
